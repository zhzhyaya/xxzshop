 import request from '@/utils/request'

 let FullreduceApi = {
   /*列表*/
    getList(data, errorback) {
        return request._post('/supplier/setting.fullreduce/index', data, errorback);
    },
    /*添加*/
    add(data, errorback) {
        return request._post('/supplier/setting.fullreduce/add', data, errorback);
    },
    /*修改*/
    edit(data, errorback) {
        return request._post('/supplier/setting.fullreduce/edit', data, errorback);
    },
    /*删除*/
    del(data, errorback) {
        return request._post('/supplier/setting.fullreduce/delete', data, errorback);
    },
}

export default FullreduceApi;
