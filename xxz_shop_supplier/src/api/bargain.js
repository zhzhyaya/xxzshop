import request from '@/utils/request'

let Bargain = {
    /*活动列表*/
    activeList(data, errorback) {
      return request._post('/supplier/activity.bargain/index', data, errorback);
    },
    getProduct(data, errorback){
       return request._get('/supplier/activity.bargain/add', data, errorback);
    },
    addProduct(data, errorback){
       return request._post('/supplier/activity.bargain/add', data, errorback);
    },
    /*我的活动列表*/
    myList(data, errorback) {
      return request._post('/supplier/activity.bargain/my', data, errorback);
    },
    /*根据主键查询*/
    detailProduct(data, errorback){
       return request._get('/supplier/activity.bargain/edit', data, errorback);
    },
    /*保存*/
    saveProduct(data, errorback){
       return request._post('/supplier/activity.bargain/edit', data, errorback);
    },
    delProduct(data, errorback){
        return request._post('/supplier/activity.bargain/del', data, errorback);
    },
}

export default Bargain;
