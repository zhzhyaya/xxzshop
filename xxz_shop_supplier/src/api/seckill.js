import request from '@/utils/request'

let SeckillApi = {
  /*秒杀活动列表*/
  activeList(data, errorback) {
    return request._post('/supplier/activity.seckill/index', data, errorback);
  },
  getProduct(data, errorback){
     return request._get('/supplier/activity.seckill/add', data, errorback);
  },
  addProduct(data, errorback){
     return request._post('/supplier/activity.seckill/add', data, errorback);
  },
  /*我的活动列表*/
  myList(data, errorback) {
    return request._post('/supplier/activity.seckill/my', data, errorback);
  },
  /*根据主键查询*/
  detailProduct(data, errorback){
     return request._get('/supplier/activity.seckill/edit', data, errorback);
  },
  /*根据主键查询*/
  saveProduct(data, errorback){
     return request._post('/supplier/activity.seckill/edit', data, errorback);
  },
  delProduct(data, errorback){
     return request._post('/supplier/activity.seckill/del', data, errorback);
  },
}
export default SeckillApi;
