<?php

namespace app\api\model\user;

use app\api\model\plus\agent\Setting as AgentSettingModel;
use app\common\model\store\Clerk as ClerkModel;
use think\facade\Cache;
use app\common\exception\BaseException;
use app\common\model\user\User as UserModel;
use app\api\model\plus\agent\Referee as RefereeModel;
use app\common\library\easywechat\AppWx;
use app\common\model\user\Grade as GradeModel;
use app\common\library\wechat\WxBizDataCrypt;
/**
 * 用户模型类
 */
class User extends UserModel
{
    private $token;

    /**
     * 隐藏字段
     */
    protected $hidden = [
        'open_id',
        'is_delete',
        'app_id',
        'create_time',
        'update_time'
    ];

    /**
     * 获取用户信息
     */
    public static function getUser($token)
    {
        $userId = Cache::get($token);
        return self::where(['user_id' => $userId])->with(['address', 'addressDefault', 'grade','supplierUser'])->find();
    }

    /**
     * 用户登录
     */
    public function login($post)
    {
        // 微信登录 获取session_key
        $app = AppWx::getApp();
        $session = $app->auth->session($post['code']);
        // 自动注册用户
        $refereeId = isset($post['referee_id']) ? $post['referee_id'] : null;
        $userInfo = json_decode(htmlspecialchars_decode($post['user_info']), true);

        $reg_source = $post['source'];
        $user_id = $this->register($session['openid'], $userInfo, $refereeId, $session, $reg_source);
        // 生成token (session3rd)
        $this->token = $this->token($session['openid']);
        // 记录缓存, 7天
        Cache::tag('cache')->set($this->token, $user_id, 86400 * 7);
        return $user_id;
    }

    /**
     * 用户登录
     */
    public function bindMobile($post)
    {
        // 微信登录 获取session_key
        $app = AppWx::getApp();
        $session = $app->auth->session($post['code']);
        $iv = urldecode($post['iv']);
        $encrypted_data = urldecode($post['encrypted_data']);
        $pc = new WxBizDataCrypt($app['config']['app_id'], $session['session_key']);
        $errCode = $pc->decryptData($encrypted_data, $iv, $data );
        if($errCode == 0){
            $data = json_decode($data, true);
            return $this->save([
                'mobile' => $data['phoneNumber']
            ]);
        }
        return false;
    }

    /**
     * 获取token
     */
    public function getToken()
    {
        return $this->token;
    }


    /**
     * 生成用户认证的token
     */
    private function token($openid)
    {
        $app_id = self::$app_id;
        // 生成一个不会重复的随机字符串
        $guid = \getGuidV4();
        // 当前时间戳 (精确到毫秒)
        $timeStamp = microtime(true);
        // 自定义一个盐
        $salt = 'token_salt';
        return md5("{$app_id}_{$timeStamp}_{$openid}_{$guid}_{$salt}");
    }

    /**
     * 自动注册用户
     */
    private function register($open_id, $data, $refereeId = null, $decryptedData = [], $reg_source = '')
    {
        //通过unionid查询用户是否存在
        $user = null;
        if (isset($decryptedData['unionId']) && !empty($decryptedData['unionId'])) {
            $data['union_id'] = $decryptedData['unionId'];
            $user = self::detailByUnionid($decryptedData['unionId']);
        }
        if (!$user) {
            // 通过open_id查询用户是否已存在
            $user = self::detail(['open_id' => $open_id]);
        }
        if ($user) {
            $model = $user;
        } else {
            $model = $this;
            $data['referee_id'] = $refereeId;
            $data['reg_source'] = 'wx';
            //默认等级
            $data['grade_id'] = GradeModel::getDefaultGradeId();
        }
        $this->startTrans();
        try {
            // 保存/更新用户记录
            if (!$model->save(array_merge($data, [
                'open_id' => $open_id,
                'app_id' => self::$app_id
            ]))
            ) {
                throw new BaseException(['msg' => '用户注册失败']);
            }
            if (!$user && $refereeId > 0) {
                // 记录推荐人关系
                RefereeModel::createRelation($model['user_id'], $refereeId);
                //更新用户邀请数量
                (new UserModel())->setIncInvite($refereeId);
            }
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw new BaseException(['msg' => $e->getMessage()]);
        }
        return $model['user_id'];
    }

    /**
     *统计被邀请人数
     */
    public function getCountInv($user_id)
    {
        return $this->where('referee_id', '=', $user_id)->count('user_id');
    }

    /**
     * 签到更新用户积分
     */
    public function setPoints($user_id, $days, $sign_conf, $sign_date)
    {
        $rank = $sign_conf['ever_sign'];
        if ($sign_conf['is_increase'] == 'true') {
            if ($days >= $sign_conf['no_increase']) {
                $days = $sign_conf['no_increase'] - 1;
            }
            $rank = ($days - 1) * $sign_conf['increase_reward'] + $rank;
        }
        //是否奖励
        if (isset($sign_conf['reward_data'])) {
            $arr = array_column($sign_conf['reward_data'], 'day');
            if (in_array($days, $arr)) {
                $key = array_search($days, $arr);
                if ($sign_conf['reward_data'][$key]['is_point'] == 'true') {
                    $rank = $sign_conf['reward_data'][$key]['point'] + $rank;
                }
            }
        }
        // 新增积分变动明细
        $this->setIncPoints($rank, '用户签到：签到日期' . $sign_date);
        return $rank;
    }

    /**
     * 个人中心菜单列表
     */
    public static function getMenus($user, $source)
    {
        $menus = [
            'address' => [
                'name' => '收货地址',
                'path' => '/pages/user/address/address',
                'icon' => 'icon-dizhi1'
            ],
            'coupon' => [
                'name' => '领券中心',
                'path' => '/pages/coupon/coupon',
                'icon' => 'icon-youhuiquan1'
            ],
            'my_coupon' => [
                'name' => '我的优惠券',
                'path' => '/pages/user/my-coupon/my-coupon',
                'icon' => 'icon-youhuiquan-'
            ],
            'agent' => [
                'name' => '分销中心',
                'path' => '/pages/agent/index/index',
                'icon' => 'icon-fenxiao1'
            ],
            'bargain' => [
                'name' => '我的砍价',
                'path' => '/pages/user/my-bargain/my-bargain',
                'icon' => 'icon-kanjia'
            ],
            'shop' => [
                'name' => '入驻店铺',
                'path' => '/pages/shop/register',
                'icon' => 'icon-dianpu'
            ],
            'my_shop' => [
                'name' => '我的店铺',
                'path' => '/pages/user/my_shop/my_shop',
                'icon' => 'icon-dianpu'
            ],
            'app_shop' => [
                'name' => '我的店铺',
                'path' => '/pages/shop/application_status',
                'icon' => 'icon-dianpu'
            ],
            'my_attention' => [
                'name' => '我的收藏',
                'path' => '/pages/user/my_attention/my_attention',
                'icon' => 'icon-shoucang'
            ],
            'my_collect' => [
                'name' => '我的关注',
                'path' => '/pages/user/my_collect/my_collect',
                'icon' => 'icon-31guanzhu1xuanzhong'
            ],
            'signin' => [
                'name' => '签到有礼',
                'path' => '/pages/plus/signin/signin',
                'icon' => 'icon-libao'
            ],
            'setting' => [
                'name' => '设置',
                'path' => '/pages/user/set/set',
                'icon' => 'icon-shezhi1'
            ],
            'scan' => [
                'name' => '扫一扫',
                'path' => 'scanQrcode',
                'icon' => 'icon-saoyisao1'
            ],
        ];
        // 判断分销功能是否开启
        if (AgentSettingModel::isOpen()) {
            $menus['agent']['name'] = AgentSettingModel::getAgentTitle();
        } else {
            unset($menus['agent']);
        }
        //判断是否入住店铺
        if($user['user_type']==1){
            unset($menus['my_shop']);unset($menus['app_shop']);
        }else if($user['user_type']==2){
            // 申请中或者已入驻成功
            unset($menus['shop']);
            // 入驻成功
            if(UserModel::isSupplier($user['user_id'])){
                unset($menus['app_shop']);
            } else{
                unset($menus['my_shop']);
            }
        }
        // 如果来源是app并且是店员，出现扫一扫
        if($source == 'app' && ClerkModel::detail(['user_id' => $user['user_id']])){

        } else {
            unset($menus['scan']);
        }
        $mens_arr = [];
        foreach ($menus as $menu){
            array_push($mens_arr, $menu);
        }
        return $mens_arr;
    }
}
