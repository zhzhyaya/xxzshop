<?php


namespace app\common\model\plus\chat;

use app\common\model\BaseModel;

/**
 * 客服消息关系模型
 */
class ChatRelation extends BaseModel
{
    protected $pk = 'relation_id';
    protected $name = 'chat_relation';

    
}