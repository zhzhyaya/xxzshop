<?php

namespace app\supplier\model\settings;

use app\common\model\settings\ReturnAddress as ReturnAddressModel;

/**
 * 退货地址模型
 */
class ReturnAddress extends ReturnAddressModel
{
    /**
     * 获取列表
     */
    public function getList($limit = 10,$shop_supplier_id)
    {
        return $this->order(['sort' => 'asc'])
            ->where('shop_supplier_id','=',$shop_supplier_id)
            ->where('is_delete', '=', 0)
            ->paginate($limit, false, [
                'query' => \request()->request()
            ]);
    }

    /**
     * 获取全部收货地址
     */
    public function getAll($shop_supplier_id)
    {
        return $this->where('shop_supplier_id', '=', $shop_supplier_id)
            ->where('is_delete', '=', 0)
            ->order(['sort' => 'asc'])
            ->select();
    }

    /**
     * 添加新记录
     */
    public function add($data)
    {
        $data['app_id'] = self::$app_id;
        return $this->save($data);
    }

    /**
     * 编辑记录
     */
    public function edit($data)
    {
        return $this->save($data);
    }

    /**
     * 删除记录
     */
    public function remove()
    {
        return $this->save(['is_delete' => 1]);
    }

}