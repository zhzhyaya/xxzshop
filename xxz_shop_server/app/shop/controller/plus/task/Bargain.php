<?php

namespace app\shop\controller\plus\task;

use app\shop\controller\Controller;
use app\shop\model\product\Product as ProductModel;
use app\shop\model\plus\task\BargainActive as ActiveModel;

/**
 * 砍价控制器
 */
class Bargain extends Controller
{
    /**
     * 砍价活动列表
     */
    public function index()
    {
        $model = new ActiveModel;
        $search = $this->postData('search/s');
        $list = $model->getList($search);
        return $this->renderSuccess('', compact('list'));
    }

    /**
     * 新增砍价活动
     */
    public function add()
    {
        $data = $this->postData();
        $model = new ActiveModel;
        // 新增记录
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功');
        }
        return $this->renderError('添加失败');
    }

    /**
     * 砍价活动详情
     */
    public function detail()
    {
        $active_id = $this->postData('active_id/i');
        // 砍价活动详情
        $detail = ActiveModel::detail($active_id);
        $product = ProductModel::detail($detail['product_id']);
        return $this->renderSuccess('', compact('detail', 'product'));

    }

    /**
     * 更新砍价活动
     */
    public function edit($active_id)
    {
        $param = $this->postData();
        $where['active_id'] = $param['active_id'];
        unset($param['active_id']);
        $model = new ActiveModel;
        // 更新记录
        if ($model->edit($param, $where)) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

    /**
     * 删除砍价活动
     */
    public function delete($active_id)
    {
        $active_id = $this->postData('active_id/i');
        // 砍价活动详情
        $model = new ActiveModel;
        if (!$model->setDelete(['active_id' => $active_id])) {
            return $this->renderError($model->getError() ?: '删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

}