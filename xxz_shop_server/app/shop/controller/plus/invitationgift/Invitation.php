<?php

namespace app\shop\controller\plus\invitationgift;

use app\shop\controller\Controller;
use app\shop\model\plus\invitationgift\Invitation as InvitationModel;
use app\common\service\qrcode\ExtensionService;
use app\shop\model\plus\invitationgift\Partake;

/**
 * Class Invitation
 * 邀请有礼控制器
 * @package app\shop\plus\invitationgift
 */
class Invitation extends Controller
{
    /*
       * 邀请列表
       */
    public function lists()
    {
        $model = new InvitationModel();
        $list = $model->getList($this->postData());
        return $this->renderSuccess('', compact('list'));
    }

    /**
     * 添加
     * @return \think\response\Json
     */
    public function add()
    {
        $model = new InvitationModel();
        if ($model->add($this->postData())) {
            return $this->renderSuccess('保存成功');
        }
        return $this->renderError('保存失败');
    }

    /**
     * 获取数据
     * @param null $id
     */
    public function getDatas($id)
    {
        $model = new InvitationModel();
        $data = $model->getDatas($id);
        return $this->renderSuccess('', compact('data'));
    }

    /**
     * 编辑
     * @param $id
     */
    public function edit()
    {
        $model = new InvitationModel();
        if ($model->edit($this->postData())) {
            return $this->renderSuccess('保存成功');
        }
        return $this->renderError('保存失败');
    }

    /**
     * 操作活动
     * @param $id
     */
    public function send($id)
    {
        $model = new InvitationModel();
        if ($model->send($id)) {
            return $this->renderSuccess('保存成功');
        }
        return $this->renderError('保存失败');
    }

    /**
     * 终止
     * @param $id
     */
    public function end($id)
    {
        $model = new InvitationModel();
        if ($model->end($id)) {
            return $this->renderSuccess('保存成功');
        }
        return $this->renderError('保存失败');
    }

    /**
     * 删除
     * @param $id
     */
    public function del($id)
    {
        $model = new InvitationModel();
        if ($model->del($id)) {
            return $this->renderSuccess('保存成功');
        }
        return $this->renderError('保存失败');
    }

    /**
     * 获取推广二维码
     */
    public function qrcode($id)
    {
        $Qrcode = new ExtensionService($id, 'invitation');
        return $this->renderSuccess('', [
            'qrcode' => $Qrcode->getImage(),
        ]);
    }

    /**
     * 参与记录
     * @param $id
     */
    public function partakelist($id)
    {
        $model = new Partake();
        $list = $model->getList($id);
        return $this->renderSuccess('', compact('list'));
    }
}