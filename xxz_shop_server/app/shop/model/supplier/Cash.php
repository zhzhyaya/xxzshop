<?php

namespace app\shop\model\supplier;

use app\common\model\supplier\Cash as SupplierCashModel;
use app\shop\model\supplier\Supplier as SupplierModel;
/**
 * 后台管理员登录模型
 */
class Cash extends SupplierCashModel
{
    /**
     * 获取列表数据
     */
    public function getList($params)
    {
        $model = $this;
        // 查询列表数据
        return $model->with(['supplier', 'account'])
            ->order(['create_time' => 'desc'])
            ->paginate($params, false, [
                'query' => \request()->request()
            ]);
    }

    /**
     * 分销商提现审核
     */
    public function submit($param)
    {
        $data = ['apply_status' => $param['apply_status']];
        if ($param['apply_status'] == 30) {
            $data['reject_reason'] = $param['reject_reason'];
        }
        // 更新申请记录
        $data['audit_time'] = time();
        $this->save($data);
        // 提现驳回：解冻分销商资金
        if ($param['apply_status'] == 30) {
            SupplierModel::backFreezeMoney($this['shop_supplier_id'], $this['money']);
        }
        return true;
    }

    /**
     * 确认已打款
     */
    public function money()
    {
        $this->startTrans();
        try {
            // 更新申请状态
            $data = ['apply_status' => 40, 'audit_time' => time()];
            $this->save($data);

            // 更新分销商累积提现佣金
            SupplierModel::totalMoney($this['shop_supplier_id'], $this['money']);
            // 事务提交
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            $this->rollback();
            return false;
        }
    }

    /**
     * 获取申请数量
     */
    public static function getApplyCount($apply_status){
        return self::where('apply_status', '=', $apply_status)->count();
    }
}